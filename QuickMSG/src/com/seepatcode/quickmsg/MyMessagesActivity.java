package com.seepatcode.quickmsg;


import com.seepatcode.quickmsg.adapters.FriendsAdapter;
import com.seepatcode.quickmsg.models.FacebookUser;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MyMessagesActivity extends FragmentActivity {   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friendslist);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);       
        final ListView friendList = (ListView) findViewById(R.id.friendList);
        FriendsAdapter fAdapter = new FriendsAdapter(this);
        friendList.setAdapter(fAdapter);        
        friendList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long arg3) {
				String userId = ((FacebookUser)friendList.getItemAtPosition(position)).userId;                        
				Intent intent = new Intent(MyMessagesActivity.this,SendMessageActivity.class);				    		
				intent.putExtra("userId", userId);
				startActivity(intent);
            	}
				
			});        
    }
}
