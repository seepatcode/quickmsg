package com.seepatcode.quickmsg.models;

public class QuickMessage {
	public String message;
	public String senderId;
	public String recipientId;
	public String date;
}
