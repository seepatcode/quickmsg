package com.seepatcode.quickmsg.models;

import java.util.List;

public class FacebookUser {
	public String userId;
	public String firstName;
	public String lastName;
	public List<Conversation> conversations;
	
	public FacebookUser(String firstName,String lastName,String id)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.userId = id;
	}
}
