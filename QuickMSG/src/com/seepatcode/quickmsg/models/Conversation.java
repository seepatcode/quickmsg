package com.seepatcode.quickmsg.models;

import java.util.List;
public class Conversation {
	public String userId;
	public List <QuickMessage> messages;
	public String firstName;
	public String lastName;
}