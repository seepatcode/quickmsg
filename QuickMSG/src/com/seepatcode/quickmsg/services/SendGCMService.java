package com.seepatcode.quickmsg.services;
/**
 * This class is run after our phone has registered
 * with the GCM server. This is how we send notifications.
 * TODO: Put this into a Wakeful Intent Service because we need
 * to make sure this completes.
 *
 * @author  Pat Cullen (pat.s.cullen@gmail.com)
 */
import android.content.SharedPreferences;

public class SendGCMService extends RestResponderServce {
   private final String action="/Application/registerGCM";

   @Override
   public void onRESTResult(int code, String result) {
       // Here is where we handle our REST response. This is similar to the 
       // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial
	   if(code==200)
	   {
		   SharedPreferences pref=this.getSharedPreferences("gcmTest", 0);
		   SharedPreferences.Editor editor = pref.edit();
	   	   editor.putBoolean("gcm_sent", true).commit();
	   }
       stopSelf();
   }

	@Override
	protected String getAction() {
		return action;
	}
}