package com.seepatcode.quickmsg;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import net.neilgoodman.android.loader.RESTLoader;
import net.neilgoodman.android.loader.RESTLoader.RESTResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.google.android.gcm.GCMRegistrar;

import android.net.Uri;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements LoaderCallbacks<RESTLoader.RESTResponse> {

	
	ProgressDialog progressDialog;
	TextView errorMessage;
	private SharedPreferences pref;
	private Bundle params = new Bundle();
	private SessionData sd = SessionData.getInstance();
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	pref=getPreferences(MODE_PRIVATE);
        super.onCreate(savedInstanceState);
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        errorMessage = (TextView) findViewById(R.id.errorMessage);
        
        //************** Setup Button Listener ****************/
        final ImageButton facebookButton = (ImageButton) findViewById(R.id.fbButton);
        facebookButton.setOnClickListener(new View.OnClickListener() {    	
    		public void onClick(View v) {
    			errorMessage.setText("");
    			fbLogin();		
    		}
    	});
    }
    /***********************************Facebook Authentication**********************************/
	 private void fbLogin()
	 {  
	       String access_token = pref.getString("access_token", null);
	       long expires = pref.getLong("access_expires", 0);
	       if(access_token != null)
	       {
		       sd.setAccessToken(access_token);
		       sd.systemId = pref.getString("QM_SystemId", null);
		       setIdFacebookUser();
	       }	       
	       if(expires != 0){
	    	   SessionData.getFacebook().setAccessExpires(expires);
	       }
	       if(!SessionData.getFacebook().isSessionValid())
	       {
	    	   //This method runs on first time as well as access token expiration
	    	   SessionData.getFacebook().authorize(this, new String[] {"read_friendlists"},Facebook.FORCE_DIALOG_AUTH, new DialogListener() 
	    	   {
		            public void onComplete(Bundle values) 
		            {
		    			progressDialog = ProgressDialog.show(MainActivity.this, "Requesting Messages", "Loading...");
		            	SharedPreferences.Editor editor = pref.edit();
		            	editor.putString("access_token", SessionData.getAccessToken()).commit();
		            	setIdFacebookUser();	
		            	registerGCM();
		     	       	requestMessages();
		            }		
		            
					public void onFacebookError(FacebookError e) {
						e.printStackTrace();
					}
					public void onError(DialogError e) {
						e.printStackTrace();
					}
					public void onCancel() {
					}
		        });
		   }
	       else
	       {
	    	   progressDialog = ProgressDialog.show(MainActivity.this, "Requesting Messages", "Loading...");
	    	   setIdFacebookUser();	
	    	   registerGCM();
    	       requestMessages();
	       }
	  }
	 /*
	  * Method only gets run when user is logging into facebook for the first time,
	  * it retrieves the user's facebook id. We will user this on the server side in combination
	  * with the access_token to authenticate the user's requests.
	  */
	 private void setIdFacebookUser() {
		 SessionData.mAsyncRunner.request("me", new RequestListener()
		 {
		     @Override
		     public void onComplete(String response, Object state) {
		        try {
		        	SharedPreferences.Editor editor = pref.edit();
		            JSONObject json = Util.parseJson(response);
		            final String id = json.getString("id");
		            editor.putString("QM_SystemId",id);
		            editor.commit();
		            SessionData.getInstance().systemId=id;
		    	    params.putString("firstName", json.getString("first_name"));
		    	    params.putString("lastName", json.getString("last_name"));
		        } 
		        catch (FacebookError e) {
		            Log.w("fb", e.toString());
		        } 
		        catch (JSONException e) {
		            Log.w("fb", e.toString());
		        }
		     }
			@Override
			public void onIOException(IOException e, Object state) {}
			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {}
			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {}
	
			@Override
			public void onFacebookError(FacebookError e, Object state) {}
		 });	
	}
	private boolean registerGCM()
	{
		SharedPreferences gcmPrefs=this.getSharedPreferences("gcmTest", 0);
		GCMRegistrar.checkDevice(this);
    	GCMRegistrar.checkManifest(this); //Only needed during dev. Once published, remove it.
    	final String regId = GCMRegistrar.getRegistrationId(this);
    	//If it's not registered or we never successfully added it to pref. Register it.
    	if (regId.equals("")||!gcmPrefs.getBoolean("gcm_sent",false)) 
    	{
    	  GCMRegistrar.register(this, Constants.GSM_SENDER_ID);
    	} else {
    	  Log.v("GCM_REG", "Already registered");
    	}
		return false;
	}
	private void requestMessages()
	{
	    this.getSupportLoaderManager().restartLoader(1, RestClient.prep("/Application/getMessages", params), this); 
	}
	/***************************Methods that get called on completion of rest calls*****************************/
	private void loadMessages(int code,String json)
	{
		if(code != 200 || json==null || !json.equals("Success"))
		{
			progressDialog.dismiss();
			TextView errorMessage = (TextView) findViewById(R.id.errorMessage);
			if(code==0)
				errorMessage.setText("Could not connect to server.");
			else
				errorMessage.setText("Received bad data from server, please try again later.");
		}
		else
		{
    		//Intent loadFriends = new Intent(StartActivity.this,LoadFriendsService.class);
    		//startService(loadFriends);
    		progressDialog.dismiss();	
		    Intent intent = new Intent(MainActivity.this,MyMessagesActivity.class);				    		
			startActivity(intent);
		}
	}

	@Override
	public Loader<RESTResponse> onCreateLoader(int id, Bundle args) {
		if (args != null && args.containsKey("URI") && args.containsKey("params")) {
            Uri    action = args.getParcelable("URI");
            Bundle params = args.getParcelable("params");     
            return new RESTLoader(this, RESTLoader.HTTPVerb.POST, action, params);
        }
        return null;
	}
	/*
     * Overridden method which is called after has completed, each loader has an ID, this is how 
     * I determine which part of the code to call
     */
	@Override
	public void onLoadFinished(Loader<RESTResponse> loader, RESTResponse data) {
        int code = data.getCode();
        String json = data.getData();
        if(loader.getId() ==1)
        	loadMessages(code,json);
	}
	@Override
	public void onLoaderReset(Loader<RESTResponse> arg0) {
		// TODO Auto-generated method stub
		
	}
}
