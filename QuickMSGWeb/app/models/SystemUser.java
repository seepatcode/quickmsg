package models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.PrePersist;
import com.google.code.morphia.annotations.Transient;
import com.google.code.morphia.query.Criteria;

import play.*;
import play.modules.morphia.Model;

@Entity("SystemUser")
public abstract class SystemUser extends Model {
    @Id public String userId;
	public String gcmKey;
	public String accessToken;
	public String firstName;
	public String lastName;
	@Transient public List<QuickMessage> messages;
	public List<QuickMessage> getMessages()
	{
		MorphiaQuery q = QuickMessage.q();	
		List<Criteria> critList = new ArrayList();		
		critList.add(q.criteria("recipientId").equal(this.userId));
		critList.add(q.criteria("senderId").equal(this.userId));
		q.or(critList.toArray(new Criteria[critList.size()]));
		return q.asList();
	}
	
    @Override
    public Object getId() {
        return userId;
    }
    
    @Override
    protected void setId_(Object id) {
    	userId = (String) processId_(id);
    }
    
    protected static Object processId_(Object id) {
        return id.toString();
    }
    
    public void notifyGCM(String content) throws IOException
    {
    	Sender sender = new Sender(Play.configuration.getProperty("GCM_KEY"));
    	Builder messageBuilder = new Message.Builder();
    	messageBuilder.addData("content", content);
    	Message message = messageBuilder.build();
    	Result result = sender.send(message, this.gcmKey, 5);
    	if (result.getMessageId() != null) {
    		String canonicalRegId = result.getCanonicalRegistrationId();
    		if (canonicalRegId != null) {
    		   // same device has more than one registration ID: update database
    			this.gcmKey=canonicalRegId;
    		}
    	} 
    	else 
    	{
    		String error = result.getErrorCodeName();
    		if (error.equals(Constants.ERROR_NOT_REGISTERED)) 
    		{
    			// application has been removed from device - unregister database
    			this.gcmKey=null;
    		}
    	}
    }
    /*
     * The commented out sections are for multicast
     */
	public void sendNotification() {
		try {
			
			Sender sender = new Sender("AIzaSyCQN_1D6sy2SuzUac0M0UX8IMGOw867Auk");

			//ArrayList<String> devicesList = new ArrayList<String>();
			//devicesList.add(this.gcmKey);

			// Use this line to send message without payload data
			// Message message = new Message.Builder().build();

			// use this line to send message with payload data
			Message message = new Message.Builder()
					.collapseKey("1")
					.timeToLive(3)
					.delayWhileIdle(true)
					.addData("message",
							"New Message!")
					.build();

			// Use this code to send to a single device
			Result result = sender.send(message, this.gcmKey, 1);

			// Use this for multicast messages
			//MulticastResult result = sender.send(message, devicesList, 1);
			//sender.send(message, devicesList, 1);
			if (result.getCanonicalRegistrationId() != null) {
				String canonicalRegId = result.getCanonicalRegistrationId();
				if (canonicalRegId.equals("0")) {
					//String error = result.getErrorCodeName();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
