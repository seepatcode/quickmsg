package models;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.code.morphia.annotations.Transient;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.types.User;

public class FacebookUser extends SystemUser {

	public FacebookUser(String uid, String access_token) {
		this.userId=uid;
		this.accessToken=access_token;
		FacebookClient facebookClient = new DefaultFacebookClient(access_token);
		User user = facebookClient.fetchObject("me", User.class);
		this.firstName=user.getFirstName();
		this.lastName=user.getLastName();
	}
	/* Calls restFB to see if this token is good for this user id*/
	public static boolean isAuthentic(String uid, String access_token) {
		FacebookClient facebookClient = new DefaultFacebookClient(access_token);
		User user = facebookClient.fetchObject("me", User.class);
		return uid!=null && uid.equals(user.getId());
	}
	@Override
	public String toString()
	{
		return this.firstName + " " + this.lastName;
	}

	public List<FacebookUser> getFriendsOnQuickMSG() {
		FacebookClient facebookClient = new DefaultFacebookClient(this.accessToken);
		Connection<User> myFriends = facebookClient.fetchConnection("me/friends", User.class);
		List<String> friendIds = new ArrayList();
		for(User friend: myFriends.getData())
		{
			friendIds.add(friend.getId());
		}
		List<FacebookUser> qmFriends = SystemUser.find().<FacebookUser>field("_id").in(friendIds).asList();
		return qmFriends;
	}
	@Transient public List<Conversation> conversations;
	public List<Conversation> getConversations()
	{
		List<FacebookUser> friends = getFriendsOnQuickMSG();
		List<Conversation> conversations = new ArrayList<Conversation>();
		for(FacebookUser friend: friends)
		{
			conversations.add(new Conversation(friend.userId));
		}
		for(QuickMessage qm: getMessages())
		{
			String userId = qm.getOtherId(this.userId);
			for(Conversation conversation: conversations)
			{
				if(userId.equals(conversation.userId))
				{
					conversation.messages.add(qm);
				}
			}
		}
		return conversations;
	}
}
