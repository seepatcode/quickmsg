package controllers;
/**
 * This is the only controller for all Android
 * REST transactions with our Play server. The setConnectedUser
 * class handles all the fun Facebook authentication so that our
 * methods are free to just get what the user is asking for
 *
 * @author  Pat Cullen (pat.s.cullen@gmail.com)
 */
import play.*;
import play.mvc.*;

import java.io.IOException;
import java.util.*;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;

import flexjson.JSONSerializer;

import models.*;

public class Application extends Controller {
	public final static String SUCCESS="Success";
	private static FacebookUser user;
	
	@Before 
    static void setConnectedUser(){
		System.out.print("Application/");
		String uid = params.get("id");
		String access_token = params.get("access_token");
		user = FacebookUser.findById(uid);
		if(user==null||!user.accessToken.equals(access_token))
		{
			if(user!=null&&!FacebookUser.isAuthentic(uid, access_token))
			{
				System.out.println("not authentic");
				response.status=400;
				renderText("NOT-AUTHENTIC");
			}
			else if(user==null)
			{
				System.out.println("Created User");
				user = new FacebookUser(uid, access_token);
			}
			else
				user.accessToken=access_token;
			user.save();	
		}
    }
    public static void login()
    {
    	System.out.println("login");
    	List<FacebookUser> friends = user.getFriendsOnQuickMSG();
    	System.out.println(friends);
    	JSONSerializer friendSerializer = new JSONSerializer().include("userId","firstName","lastName").exclude("*"); 
    	System.out.println("JSON="+friendSerializer.serialize(friends));
    	renderJSON(friendSerializer.serialize(friends));
    }
    public static void getMessages() 
    {
    	System.out.println("getMessages");
    	List<QuickMessage> messages = user.getMessages();
    	Collections.sort(messages);
    	System.out.println(messages);
    	JSONSerializer messageSerializer = new JSONSerializer().include("senderId","message","recipientId","date").exclude("*"); 
    	System.out.println("JSON="+messageSerializer.serialize(messages));
    	renderJSON(messageSerializer.serialize(messages));
    }
    public static void getConversations()
    {
    	System.out.println("getConversation");
    	JSONSerializer convoSerializer = new JSONSerializer().include("userId","conversations.userId",
    			"conversations.messages.senderId","conversations.messages.message", "conversations.firstName",
    			"conversations.lastName","firstName","lastName").exclude("*"); 
    	System.out.println("JSON="+convoSerializer.serialize(user));
    	renderJSON(convoSerializer.serialize(user));
    }
    public static void registerGCM(String gcm_key)
    {
    	System.out.println("registerGcm");
    	user.gcmKey=gcm_key;
    	user.save();
    	renderText(SUCCESS);
    }
    public static void sendMessage(JsonObject json)
    {
    	System.out.println("sendMessage");
    	System.out.println("Received JSON: " + json);
    	Gson gson = new Gson();
		QuickMessage message = new QuickMessage();
		if(json!=null)
		{
			message = gson.fromJson(json, QuickMessage.class);
		   	message.save();
		   	renderText("SUCCESS");
		}
    	response.status=400;
    	renderJSON("Bad Data!");
    }
    public static void sendMessage2(JsonObject data)
    {
    	System.out.println(data);
    	response.status=400;
    	renderText("NOT YET IMPLEMENTED!");    	
    }
}