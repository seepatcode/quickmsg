package controllers;
/**
 * This is the only controller for send test messages via
 * Web. To access it locally, hit localhost:9000/TestController/index.
 * This is a useful approach when coding interactions between two devices.
 * 
 * @author  Pat Cullen (pat.s.cullen@gmail.com)
 */
import models.FacebookUser;
import models.QuickMessage;
import models.SystemUser;
import play.mvc.*;

public class TestController extends Controller {

    public static void index() {
        render();
    }

    public static void sendNotification(String userId)
    {
    	if(userId==null)
    		index();
    	SystemUser user = SystemUser.findById(userId);
    	user.sendNotification();
    	renderArgs.put("message", "Notification to " + user + " sent.");
    	renderTemplate("TestController/index.html");
    }
    public static void sendMessage(String senderId, String recipientId, String message)
    {
    	SystemUser sender = SystemUser.findById(senderId);
    	SystemUser recipient = SystemUser.findById(recipientId);
    	QuickMessage qm = new QuickMessage(sender, recipient, message);
    	qm.save();
    	renderArgs.put("message", "Sent: " + message + "\nFrom: " + sender + "\nTo: " + recipient);
    	renderTemplate("TestController/index.html");
    }
}
